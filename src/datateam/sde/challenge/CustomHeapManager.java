package datateam.sde.challenge;

import java.io.IOException;

import datateam.sde.challenge.exception.CustomHeapManagerException;

public interface CustomHeapManager {
	public boolean saveObject(String objName, Object obj) throws CustomHeapManagerException, IOException, ClassNotFoundException;
	public Object retrieveObject(String objName) throws CustomHeapManagerException;
	public void deleteObject(String objName) throws CustomHeapManagerException;
}
