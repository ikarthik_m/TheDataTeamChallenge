package datateam.sde.challenge;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import datateam.sde.challenge.exception.CustomHeapManagerException;

public class CustomHeapManagerImpl implements CustomHeapManager {
	
	HashMap<String,Object> HeapManagerMap;
	String filePath;
	
	public CustomHeapManagerImpl(String filePath)throws IOException{
		HashMap<String,Object> HeapManagerMap = new HashMap<String,Object>();
		this.filePath = filePath;
		try(FileOutputStream fout = new FileOutputStream(filePath);ObjectOutputStream oos = new ObjectOutputStream(fout)){
			oos.writeObject(HeapManagerMap);
		}
	}

	@Override
	public boolean saveObject(String objName, Object obj) throws CustomHeapManagerException, IOException, ClassNotFoundException {
		try(FileOutputStream fout = new FileOutputStream(filePath);ObjectOutputStream oos = new ObjectOutputStream(fout)){
			HashMap<String, Object> hmap = this.getRootObject();
			hmap.put(objName, obj);
			oos.writeObject(hmap);
		}catch(IOException ex){
			throw new CustomHeapManagerException(ex);
		}catch(ClassNotFoundException ex){
			throw new CustomHeapManagerException("An internal error occured", ex);
		}
		return true;
	}

	@Override
	public Object retrieveObject(String objName) throws CustomHeapManagerException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteObject(String objName) throws CustomHeapManagerException {
		// TODO Auto-generated method stub

	}
	
	private HashMap<String,Object> getRootObject() throws IOException, ClassNotFoundException{
		HashMap<String,Object> hm = new HashMap<String, Object>();
		try(FileInputStream fin = new FileInputStream(filePath);
			ObjectInputStream ois = new ObjectInputStream(fin)){
			hm = (HashMap<String, Object>) ois.readObject();
		}catch(EOFException ex){
			//Do nothing
		}
		return hm;
	}

}
