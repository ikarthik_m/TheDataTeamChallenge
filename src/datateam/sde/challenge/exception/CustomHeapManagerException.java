package datateam.sde.challenge.exception;

public class CustomHeapManagerException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	CustomHeapManagerException(){
		super();
	}
	public CustomHeapManagerException(String message){
		super(message);
	}
	public CustomHeapManagerException(Throwable cause){
		super(cause);
	}
	public CustomHeapManagerException(String message, Throwable cause){
		super(message, cause);
	}
}
