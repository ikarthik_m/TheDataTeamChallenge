package datateam.sde.challenge.tests;

import java.io.IOException;

import datateam.sde.challenge.CustomHeapManager;
import datateam.sde.challenge.CustomHeapManagerImpl;
import datateam.sde.challenge.exception.CustomHeapManagerException;

public class SampleTests {

	public static void main(String[] args) throws IOException, ClassNotFoundException, CustomHeapManagerException {
		CustomHeapManager chm = new CustomHeapManagerImpl("fileObj.txt");
		String name = "Karthikeyan";
		System.out.println(chm.saveObject("name", name));
	}

}