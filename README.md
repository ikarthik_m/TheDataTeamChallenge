# Custom Heap Manager

This java project gives Custom heap manager API to store, retrieve and delete objects by name.

#### Idea

Initially, a hashmap object is initialized with String(object name) and Object as key value pairs.
This object is used to store/update, read and delete objects. 3 API end points are given via CustomHeapManager interface.

Garbage collection can also be implemented with mark and sweep feature, that will require further more tweak into data structure.

#### Performance

HashMap implementation is done using hash tables and balanced binary search tree. It will take constant time to store and O(log n) time to read objects. Deletion will also take constant time.
This will impact in performance as number of objects goes higher, like 100, 1000000, 10^9.

Time taken:

100 = 0.001 seconds

100,000 = 0.051 seconds

1,000,000 = 1.3 seconds


You can check the time taken for number of iterations(n) by changing variable "i" value in RuntimeAnalysis source file under tests package.

#### Tests

Unit test is written using Junit4 framework. I have covered trivial use cases in testing.

#### Usage

To run, it is necessary to have,

JDK 1.8
Maven 2.3+(for Junit4 jar)

Import the folder as java project into Eclipse or IntelliJ and run mvn clean install.

source code is found under datateam.sde.challenge package.
tests are found under datateam.sde.challenge.tests package.

#### Contact

Please write to me for further discussion:

karthikeyanisaquizer@gmail.com
+91 9487661207 
